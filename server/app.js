var express = require("express");
var nodeMailer = require("nodemailer");
var bodyParser = require("body-parser");
var request = require("request");
var path = require("path");
var favicon = require("serve-favicon");
const fetch = require("isomorphic-fetch");
const { Client } = require("pg");

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(
  "/assets",
  express.static(path.join(__dirname, "client", "dist", "assets"))
);
app.use(express.static(__dirname + "/client/dist"));

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/client/dist/index.html");
});

app.use(favicon(__dirname + "/client/dist/favicon.ico"));

const handleSubmit = (req, res) => {
  const secret_key = "6LcD4sYUAAAAALDrxEkWMaRytJBr2YHmgZM1PFvm";
  const token = req.body.token;
  const email = req.body.email;
  const uname = req.body.uname;
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`;

  fetch(url, {
    method: "post"
  })
    .then(response => response.json())
    .then(google_response => {
      if (google_response.success) {
        let transporter = nodeMailer.createTransport({
          service: "gmail",
          auth: {
            // should be replaced with real sender's account
            user: "email242342@gmail.com",
            pass: "8veB89n52ux7UDM"
          }
        });

        const mailOptions = {
          // should be replaced with real recipient's account
          from: "email242342@gmail.com", // sender address
          to: "alex-wd@yandex.ru, sales@upsaleslab.com", // list of receivers
          subject: "Upsaleslab.com: New user registration", // Subject line
          html: `Upsaleslab.com: New user registration<br> ${email} - ${uname}` // plain text body
        };

        transporter.sendMail(mailOptions, function(err, info) {
          if (err) console.log(err);
          //else console.log(info);
        });
      }
      res.json({ google_response });
    })
    .catch(error => res.json({ error }));
};

app.post("/submit", handleSubmit);

app.use("*", function(req, res) {
  res.status(404).send("404");
});

app.listen(port, () => console.log(`Listening on port ${port}`));
